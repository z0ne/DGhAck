import r2pipe, os

def main():
	directory = "samples"

	f_str = "0x004040bf 6369 7869 6369 7869 28b2 b2b2 b2b2 b2b2 cixicixi(......."

	for filename in os.listdir(directory):
		r2_directory = "samples/" + filename
		
		r = r2pipe.open(r2_directory)
    
		res = ["", "", "", ""]
    
		res[0] = r.cmd("i~nx")
		res[1] = r.cmd("pi 1")
		res[2] = r.cmd("ph md5 0x20 @ 0x0040404f")
		res[3] = r.cmd("pox 22 @ 0x004040bf~[0-10]~:1")

		for i in range (len(res)):
			res[i] = res[i].rstrip("\n")

		hint = 0

		if res[0] == "nx       false":
			print("[+] nx stack false!")
			hint += 1
        
			if res[1] == "xor ebp, ebp":
				print("[+] good first d instrusction!")    
				hint += 1
	
				if res[2] == "f3ea40bcc61066261ea3a018560434e2":
					print("[+] md5 hash block valid!")
					hint += 1

					if res[3] == f_str:
						print("[+] found hex pattern!")
						hint += 1
					else:
						continue 
				else:
					continue
			else:
				continue
       

		if hint == 4:
			print("FOUND IT:" + filename)
			flag = os.system("./samples/sample256")
			break
main()		
