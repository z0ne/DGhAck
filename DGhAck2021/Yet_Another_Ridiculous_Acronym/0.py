import r2pipe, os, json

directory = "samples"

f = open("random.txt", "a")

l = list(range(10, 100))

o = [str(a) for a in l]

with open("madd.json") as myjson:
        json_data = json.load(myjson)
        
        for filename in os.listdir(directory):
            r2_directory = "samples/" + filename
    
            r = r2pipe.open(r2_directory)
    
            res = ["", "", ""]

            res[0] = r.cmd("i~nx")
            res[1] = r.cmd("pi 1")
            res[2] = r.cmd("ph md5 0x20 @ 0x0040404f")

            for i in range (len(res)):
                res[i] = res[i].rstrip("\n")

            hint = 0

            if res[0] == "nx       false":
                #print("[+] nx stack false!")
                hint += 1

                if res[1] == "xor ebp, ebp":
                    #print("[+] good first d instrusction!")    
                    hint += 1
                    
                    if res[2] == "f3ea40bcc61066261ea3a018560434e2":
                        #print("[+] md5 hash block valid!")
                        hint += 1

                        for i in range(0, 256):  
                            for j in range(0, 90):
                                str = r.cmd("pox " + o[j] + " @ " + json_data[i])
                                f.write(filename + "\n" + str + "\n")

                    else:
                        continue
                else:
                    continue            
            else:
                continue
        
    

f.close()

		