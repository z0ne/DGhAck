# Secure FTP Over UDP: Documentation

## General

The server listens on the UDP port 4445. It is able to process messages up to 2048 bytes. Beyond that, an error message
message will be returned.

Due to the peculiarities of UDP, it is possible that packets are ignored by the server, or that the content of the packet is altered in transit.

A guest account can be used for your tests:
- Username = `GUEST_USER
- Password = `GUEST_PASSWORD`.

The third and last flag is located in a sub-folder of `/opt/` on the server.

## Structure of a package 

A package is composed of 4 sections:

### Header

The header is composed of two bytes. 

The 14 most significant bits are the packet ID. 
The 2 least significant bits are the size in bytes of the `size' section.

### Size

The size section has a variable length from 1 to 3 bytes. It defines the length of the `content` section in bytes.

### Content

The content of the packet. This is specific to each packet.


### CRC32

Checksum stored on 4 bytes allowing to detect a possible modification of the packet during the transit.

The algorithm used is the same as the one present in Java (`java.util.zip.CRC32`).

The sum is calculated with the concatenation of the header, size and content sections.

## Protocol

The protocol is structured as **messages**. Each message has its own identifier and content. The content of each message will be detailed later in this document.

Each message you send may be answered by an `ErrorMessage` containing an error code. You must check that each message received is not an error message before processing it.

To communicate with this server, it is necessary to follow the following steps in order:

* Establishing a session:
    - Sending a `ConnectMessage`. This message must have the string `CONNECT` in its `data` attribute.
    - The server will respond to this message by sending a `ConnectReply` response containing your session ID and the first flag.
* Authentication:
    - Sending an `RsaKeyMessage` message with your session id as the `sessionId` argument.
    - The server will reply with the `RsaKeyReply` response containing its RSA public key (`servPubKey`). This key is encrypted with the XOR algorithm and the `ThisIsNotSoSecretPleaseChangeIt` key, and then encoded in Base64.
    - Sending a `SessionKeyMessage`. This message contains your session ID and a 256-bit AES key (`AES/CBC/PKCS5Padding` algorithm) that you generated. This key must be encrypted with `servPubKey` and Base64 encoded.
    - The server will respond with the `SessionKeyReply` response. This message contains a 10 byte salt in the form of a byte array encrypted with your Base64 encoded AES key.
    - Sending an `AuthMessage`. This message contains:
        - Your session ID;
        - The salt encrypted with the Base64 encoded AES key;
        - Your username encrypted with the Base64 encoded AES key;
        - Your password encrypted with the Base64 encoded AES key.
    - The server will respond with an `AuthReply` response containing the message `AUTH_OK` if authentication was successful and the second flag.
* Usage:
    * Sending a `GetFilesMessage` to list the files in a directory. This message contains your session ID and the path to list encrypted with your AES key and encoded in Base64.
        - The server will respond with the `GetFilesReply` response, which contains the names of the files in the directory as an array of strings encrypted with your AES key and Base64 encoded.
    * Sending a `GetFileMessage` to retrieve the contents of a file. This message contains your session ID and the path to the file you want to retrieve, encrypted with your AES key and encoded in Base64.
        - The server will respond with the `GetFileReply` response which contains the contents of the file encrypted with your AES key and Base64 encoded.
    
## Serialization format

* Strings and 'simple' arrays are serialized in the following form:
    - 2 bytes containing the length of the string or array.
    - The string or array.
* A string array is serialized in the following way: 
    * The contents of the array are concatenated with the NULL character (``0`) as a delimiter.
    * The result of the concatenation is treated as a string and is serialized as detailed above.

## String encoding

Strings are encoded in UTF-8.

## AES Encryption

All AES encrypted messages must begin with the initialization vector (IV) used.

## Messages 

Here is an exhaustive list of the different messages. The attributes are listed **in their serialization order**.

### RsaKeyMessage (ID : 78)
- sessionId : String
### PingMessage (ID : 10)
- pingData : String
### AuthMessage (ID : 4444)
- sessionId : String
- salt : String
- user : String
- pass : String
### GetFileMessage (ID : 666)
- sessionId : String
- path : String
### GetFilesMessage (ID : 45)
- sessionId : String
- path : String
### ErrorMessage (ID : 1)
- error : String
### ConnectMessage (ID : 1921)
- data : String
### SessionKeyMessage (ID : 1337)
- sessionId : String
- aesKey : String
### RsaKeyReply (ID : 98)
- publicKey: String
### ConnectReply (ID: 4875)
- sessionID : String
- flag : String
### PingReply (ID : 11)
- pingData : String
### SessionKeyReply (ID : 1338)
- salt : String
### AuthReply (ID : 6789)
- status : String
- flag : String
### GetFileReply (ID : 7331)
- fileContent : String
### GetFilesReply (ID : 46)
- files : String
