from base64 import b64decode, b64encode
from pwn import *

import zlib, re, code

from subprocess import PIPE, run

r = remote('secure-ftp.dghack.fr', 4445, typ="udp")

## Structure of our ConnectMessage
header_cmsg = b"\x1e\x05"
size_bytes_cmsg = b"\x09"
content_length_cmsg = b"\x00\x07"
content_s_cmsg = "CONNECT".encode(encoding="UTF-8", errors="strict")
packet_cmsg = header_cmsg + size_bytes_cmsg + content_length_cmsg  + content_s_cmsg
checksum_cmsg = b"\xcf\x9d\xae\x67"
packet_cmsg += checksum_cmsg

print("[*] ConnectMessage packet: " + str(packet_cmsg))

# Make a variable outside the func to hold the result
res = ""

with context.local(log_level='debug'):
    ## Send/reply for ConnectMessage 
    r.send(packet_cmsg)

    res = r.recv(2048)

    if b"\x4c\x2d" in res:
        print("[+] Received a ConnectReply packet!")

    print("[*] ConnectReply packet: " + str(res))
        
    if b"\x44\x47\x41" in res:
        flag = ""
            
        for i in range(43, 68):
            flag += chr(res[i])
            
        print("[+] Flag found in the packet!: " + str(flag))   
    else:
        print("[-] Didn't received a ConnectReply packet")
        r.close()


## Get the sessionId from the ConnectReply packet
sessionId = ""

for i in range(5, 41):
    sessionId += chr(res[i])

print("[*] sessionId of the ConnectReply packet: " + str(sessionId))

## Structure of our RsaKeyMessage
header_rkmsg = b"\x01\x39"
size_bytes_rkmsg = b"\x26"
content_length_rkmsg = b"\x00\x24"
content_s_rkmsg = sessionId.encode(encoding="UTF-8", errors="strict")
packet_rkmsg = header_rkmsg + size_bytes_rkmsg + content_length_rkmsg  + content_s_rkmsg

checksum_build =  hex(zlib.crc32(packet_rkmsg) & 0xffffffff)

checksum_rkmsg = p32(int(checksum_build, 16), endian="big") 

packet_rkmsg += checksum_rkmsg

print("[*] RsaKeyMessage packet: " + str(packet_rkmsg))

with context.local(log_level='debug'):
    ## Send/reply for RsaKeyMessage
    r.send(packet_rkmsg)

    res = r.recv(2048)

    if b"\x01\x8a" in res:
        print("[+] Received a RsaKeyReply packet!")
    else:
        print("[-] Didn't received a RsaKeyReply packet")
        r.close()

    print("[*] RsaKeyReply packet: " + str(res))